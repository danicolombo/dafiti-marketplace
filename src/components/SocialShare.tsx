import React from "react";
import {Grid, IconButton} from "@material-ui/core";
import FacebookIcon from '@material-ui/icons/Facebook';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import TwitterIcon from '@material-ui/icons/Twitter';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    socialButton: {
        color: 'grey',
        margin: theme.spacing(1),
    },
    container: {
        textAlign: 'center',
    }
}));

const SocialShare = () => {
    const classes = useStyles();

    return (
        <Grid container>
            <Grid item className={classes.container} xs={12} >
                <IconButton aria-label="go to facebook">
                    <FacebookIcon className={classes.socialButton}/>
                </IconButton>
                <IconButton aria-label="go to twitter">
                    <TwitterIcon className={classes.socialButton} />
                </IconButton>
                <IconButton aria-label="go to whatsapp">
                    <WhatsAppIcon className={classes.socialButton}/>
                </IconButton>
            </Grid>
        </Grid>
    );
};

export default SocialShare;


