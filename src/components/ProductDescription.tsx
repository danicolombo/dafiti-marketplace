import React from 'react';
import {IconButton, Typography, Grid, Link} from '@material-ui/core';
import FavoriteIcon from '@material-ui/icons/Favorite';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    links: {
        color: 'darkturquoise',
        fontWeight: 400,
    },
}));

const ProductDescription: React.FC<{
    product: any;
}> = ({ product }) => {
    const classes = useStyles();
    const [like, setLike] = React.useState(true);
    const preventDefault = (event: any) => event.preventDefault();
    const handleClick = () => {
        setLike(!like);
    };

    return (
        <Grid container spacing={2}>
            <Grid item xs={10}>
                <Typography variant="h4" color="textSecondary" component="p">
                    {product.brand}
                </Typography>
                <Typography variant="h5" color="textSecondary" component="p">
                    {product.productname}
                </Typography>
                <Typography variant="h6" color="textSecondary" component="p">
                    Vendido y entregado por <Link href="https://www.mannarino.com.ar/" onClick={preventDefault} className={classes.links}>{product.brand}</Link>
                </Typography>
            </Grid>
            <Grid item xs={2}>
                <IconButton aria-label="add to favorites" onClick={handleClick}>
                    {like ? <FavoriteBorderIcon /> : <FavoriteIcon />}
                </IconButton>
            </Grid>
        </Grid>
    );
}

export default ProductDescription;