import React from 'react';
import {Card, CardMedia, CardContent, Divider} from '@material-ui/core';
import ProductDescription from './ProductDescription';
import Expandable from './Expandable';
import {makeStyles} from '@material-ui/core/styles';
import SizeSelector from './SizeSelector';
import SocialShare from './SocialShare';

const useStyles = makeStyles((theme) => ({
    media: {
        height: 0,
        padding: '56.25% 0px', // 16:9
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'contain',
    },
    divider: {
        margin: `${theme.spacing(1)}px 0px`,
    },
    box: {
        padding: `0px ${theme.spacing(2)}px`,
    },
}));

const ItemCard: React.FC<{
    product: any;
}> = ({ product }) =>{
    const classes = useStyles();

    return (
        <Card>
            <CardMedia
                className={classes.media}
                image={product.otherimages[0]}
                title={product.productname}
            />
            <CardContent className={classes.box}>
                <ProductDescription product={product}/>
                <Divider className={classes.divider}/>
                <SizeSelector product={product} />
                <SocialShare />
            </CardContent>
            <Expandable title={'Descripcion'} body={product.description}/>
            <Expandable title={'Detalle'} body={product.page_category_names}/>
        </Card>
    );
}

export default ItemCard;
