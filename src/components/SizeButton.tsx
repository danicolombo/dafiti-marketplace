import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    size: {
        backgroundColor: theme.palette.primary.main,
        height: '40px',
        fontSize: '12px',
        margin: theme.spacing(1),
        fontWeight: 'bold',
    }
}));

const SizeButton: React.FC<{
    size: string;
}> = ({ size }) => {
    const classes = useStyles();

    return (
        <IconButton aria-label="size pick" className={classes.size}>
            {size}
        </IconButton>
        );
    }
export default SizeButton;