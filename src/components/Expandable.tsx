import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import { Divider } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    paragraph: {
        textTransform: 'uppercase',
        justifyContent: 'center',
        flexDirection: 'column',
        display: 'flex',
    },
    header: {
        margin: `${theme.spacing(1)}px 0px`,
        borderBottom: '1px solid #f0f0f0',
    }, 
    actions: {
        padding: `0px ${theme.spacing(2)}px`,
    }
}));

const Expandable: React.FC<{
    title: string;
    body: string;
}> = ({ body, title }) => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    return (
        <>
            <CardActions disableSpacing className={classes.actions}>
                <Grid container className={classes.header}>
                    <Grid item xs={10} className={classes.paragraph}>
                        <Typography variant="body2" color="textSecondary" component="p">{title}</Typography>
                    </Grid>
                    <Grid item xs={2}>
                        <IconButton
                            className={clsx(classes.expand, {
                                [classes.expandOpen]: expanded,
                            })}
                            onClick={handleExpandClick}
                            aria-expanded={expanded}
                            aria-label="show more"
                        >
                        <ExpandMoreIcon />
                        </IconButton>
                    </Grid>
                </Grid>
            </CardActions>
            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Typography variant="body2" color="textSecondary" component="p">{body}
                    </Typography>
                </CardContent>
            </Collapse>
        </>
    );
}
export default Expandable;