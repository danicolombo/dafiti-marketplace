import React from 'react';
import {Typography, Grid} from '@material-ui/core';
import SizeButton from './SizeButton';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    slider: {
        display: 'flex',
        flexWrap: 'nowrap',
        overflowY: 'auto',
        scrollbarColor: `grey ${theme.palette.primary.main}`,
        scrollMarginTop: '1px',
        scrollbarWidth: 'thin',
        padding: `${theme.spacing(1)}px 0px`,
    },
}));

const SizeSelector: React.FC<{
    product: any;
}> = ({ product }) => {
    const classes = useStyles();

    return (
        <Grid container spacing={3}>
            <Grid item xs={12}>
                <Typography variant="subtitle1" color="textSecondary" component="p">Talle</Typography>
                <Grid container>
                    <Grid item xs={12} className={classes.slider}>
                        {product.sizes.map((value: any) => (
                            <SizeButton size={value.size} />
                        ))}
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default SizeSelector;


