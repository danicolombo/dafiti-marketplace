import React from 'react';
import {Grid} from '@material-ui/core';
import {ThemeProvider} from '@material-ui/core/styles';
import theme from './theme';
import ItemCard from './components/ItemCard';
import data from './assets/detalle.json';

const App = () => {  
  return (
    <ThemeProvider theme={theme}>
        <Grid container justify="center" alignItems="center">
            <Grid item xs={2}>
                {data.products.map((product: any) => (
                  <ItemCard product={product} />
                ))}
            </Grid>
        </Grid>
    </ThemeProvider>
  );
}

export default App;
