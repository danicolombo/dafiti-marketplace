import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';

const dafitiGrey = "#ECECEC";
const dafitiViolet = "#C49AFE";


let theme = createMuiTheme({
    palette: {
        primary: {
            main: `${dafitiGrey}`,
        },
        secondary: {
            main: `${dafitiViolet}`,
        },
        background: {
            default: "#0B72B9",
        }
    },
    typography: {
        h4: {
            fontWeight: 500,
            fontFamly: 'Raleway',
            fontSize: '24px',
            paddingTop: '5px',
        },
        h5: {
            fontWeight: 400,
            fontFamly: 'Raleway',
            fontSize: '14px',
            paddingTop: '5px',
        },
        h6: {
            fontWeight: 400,
            fontFamly: 'Raleway',
            fontSize: '12px',
            paddingTop: '5px',
        },
        body2: {
            fontSize: '12px',
            fontWeight: 600,
            fontFamly: 'Raleway',
            paddingTop: '5px',
        },
    },
});

theme = responsiveFontSizes(theme);

export default theme;
